//
//  demo_repoApp.swift
//  demo-repo
//
//  Created by Marco Antonio Reyes Castro on 23/11/23.
//

import SwiftUI

@main
struct demo_repoApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
